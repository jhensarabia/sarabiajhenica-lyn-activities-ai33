import Vue from "vue";
import router from "./routes/router";
import App from "../src/App.vue";


import './assets/css/sidebar.css'
import './assets/css/style.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");

