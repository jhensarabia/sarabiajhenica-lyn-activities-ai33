
var ctx = document.getElementById('myBar').getContext('2d');
var myBar = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['January', 'February', 'March', 'April', 'May'],
    datasets: [{
      label: 'Borrowed',
      data: [50, 80, 30, 20, 60, 90],
      backgroundColor: '#007bff',
    },
    {
      label: 'Returned',
      data: [20, 60, 30, 10, 40, 90],
      backgroundColor: '#fd7e14',
    }
  ],
  },
  options: {
	  legend: {
      position: "bottom",
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var ctx2 = document.getElementById("myPie").getContext("2d");
var myPie = new Chart(ctx2, {
  type: "doughnut",
  data: {
    labels: ["IT Books", "History", "Reseach", "Math", "MAPEH"],
    datasets: [
      {
        data: [37,22, 10, 15, 20],
        backgroundColor: [
          "#007bff",
		  "#fd7e14",
          "##6c757d",
          "#ffc107",
          "#0E0067"
          ,
        ],
      },
    ],
  },
  
  options: {
    responsive: true,
    legend: {
      position: "bottom",
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
  },
});


function myFunction() {
  var txt;
  var r = confirm("Are You Sure?");
  
  document.getElementById("cool").innerHTML = txt;
}

