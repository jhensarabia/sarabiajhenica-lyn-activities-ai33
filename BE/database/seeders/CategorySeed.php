<?php

namespace Database\Seeders;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['category' => 'Lovelife'],
            ['category' => 'Romance'],
            ['category' => 'Mystery'],
            ['category' => 'Romance'],
        ];

        foreach ($category as $categ) {
            Category::create($categ);
        }
        
    
    }
}

